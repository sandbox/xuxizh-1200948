// JavaScript Document
if(Drupal.jsEnabled) {	
	$(document).ready(function(){
        $(".field-china-profile-province select").change(function(){
            var curr_pro = $(this).val();
             $('.field-china-profile-city select').fadeOut('slow');
             $('.field-china-profile-county select').fadeOut('slow');
             
             $.ajax({
                url: Drupal.settings.china_location_cck.fetchRegionUrl + curr_pro,
                type: "GET", 
                dataType: "json",
                timeout: 6000, 
                error: function(xhr, status, error){
                	alert('error=' + error + '请重新选择');
                	$('.field-china-profile-city select').fadeIn('slow');
                            },
                success: function(data){   
                        var selectedCity; 
                        $('.field-china-profile-city select').find('option').remove();
                        $.each(data.data, function (key ,value) {
                        $('.field-china-profile-city select').append(
                            $('<option></option>').val(key).html(value)                            
                        );
                        selectedCity = key;
                        });
                         $('.field-china-profile-city select').fadeIn('slow');
                        
                         
                         $('.field-china-profile-city select').val(selectedCity);
                          $.ajax({
                            url: Drupal.settings.china_location_cck.fetchRegionUrl + selectedCity,
                            type: "GET", 
                            dataType: "json",
                            timeout: 6000, 
                            error: function(xhr, status, error){
                            	alert('error=' + error + '请重新选择');
                                },
                            success: function(data){   
                        $('.field-china-profile-county select').find('option').remove();
                        $.each(data.data, function (key ,value) {
                        $('.field-china-profile-county select').append(
                            $('<option></option>').val(key).html(value)
                        );
                        });
                            $('.field-china-profile-county select').fadeIn('slow');
                        
                    },                
                });
            
                         
                    },                
                });
                
        });
        
        $(".field-china-profile-city select").change(function(){
            var curr_pro = $(this).val();
              $('.field-china-profile-county select').fadeOut('slow');
             $.ajax({
                url: Drupal.settings.china_location_cck.fetchRegionUrl + curr_pro,
                type: "GET", 
                dataType: "json",
                timeout: 6000, 
                error: function(xhr, status, error){
                           alert('error=' + error + '请重新选择');
                            },
                success: function(data){   
                        $('.field-china-profile-county select').find('option').remove();
                        $.each(data.data, function (key ,value) {
                        $('.field-china-profile-county select').append(
                            $('<option></option>').val(key).html(value)
                        );
                        });

                     $('.field-china-profile-county select').fadeIn('slow');   
                    },                
                });
                
        });
        
        
    });
}
